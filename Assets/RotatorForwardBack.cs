﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class RotatorForwardBack : MonoBehaviour {
    
    public float rotationAngle = 180f;
    public float rotationTime;
    private float startAngle;

    private float rotationCyclePosition;
    private float newAngle;

    private Clock clock;

    // Use this for initialization
    void Start () {

        startAngle = transform.rotation.z;

        clock = Timekeeper.instance.Clock("Enemies");
    }
	
	// Update is called once per frame
	void Update () {


        float clockTime = clock.time;

        float roatationCycles = clockTime / (rotationTime * 2);
        rotationCyclePosition = roatationCycles - Mathf.Floor(roatationCycles);

        if (rotationCyclePosition < 0.5)
        {
            newAngle = startAngle + rotationAngle * rotationCyclePosition * 2;
        }
        else
        {
            newAngle = startAngle + rotationAngle - (rotationAngle * (rotationCyclePosition - 0.5f) * 2);
        }


        //transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, newAngle, transform.rotation.w);
        transform.localEulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, newAngle);

    }
}
