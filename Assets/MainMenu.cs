﻿using UnityEngine;

public class MainMenu : MonoBehaviour {

    public GameObject menu;

	// Use this for initialization
	void Start () {

        menu.SetActive(false);

	}
	
	// Update is called once per frame
	void Update () {

        if (Input.anyKey || Input.touchCount > 0)
            {

            if (PlayerPrefs.HasKey("LevelReached"))
            {
                menu.SetActive(true);
            }
            else
            {
                //if its the first time the game is played, go straight into first level instead of options to Resume, etc.
                ChangeScene pstats = GetComponent<ChangeScene>();
                pstats.ChangeToScene("Next");
            }

        }
          

	}
}
