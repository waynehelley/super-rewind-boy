﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class ReverseGravityPad : MonoBehaviour {

    private GameObject AudioSource;
    private AudioSource noise;
    private GameObject[] Crates;
    private GravityReverseObject ReverseGravityClass;    

    void Awake()
    {
        AudioSource = GameObject.FindGameObjectWithTag("ReverseGravityNoise");
        noise = AudioSource.GetComponent<AudioSource>();
        Crates = GameObject.FindGameObjectsWithTag("Crate");        
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Player")
        {

            Debug.Log("Player Enter Reverse Gravity Pad");

            if (Timekeeper.instance.Clock("Enemies").localTimeScale == 1)
            {
                foreach (GameObject crate in Crates)
                {

                    GravityReverseObject ReverseGravityClass = crate.GetComponent<GravityReverseObject>();
                    ReverseGravityClass.ReverseGravityOnObject();

                }

                noise.Play();
            }

        }
    }


    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {

            Debug.Log("Player Exit Reverse Gravity Pad");

            foreach (GameObject crate in Crates)
            {

                GravityReverseObject ReverseGravityClass = crate.GetComponent<GravityReverseObject>();
                ReverseGravityClass.NormalGravityOnObject();

            }

        }
    }
}
