﻿using UnityEngine;
using System.Collections;

public class PlatfromGlow : MonoBehaviour {

    private SpriteRenderer BlurBorder;
    private Color BlurBorderColour;

    // Use this for initialization
    void Start () {

        BlurBorder = GetComponent<SpriteRenderer>();
        BlurBorderColour = BlurBorder.color;

        StartCoroutine(TimeImuneGlow());

    }

    IEnumerator TimeImuneGlow()
    {

        int intervals = 10;
        float time = 0.5f;
        float maxalpha = 0.5f;

        while (true) //always
        {

            for (int x = 1; x <= intervals; x++)
            {
                yield return new WaitForSeconds((time / intervals));
                BlurBorderColour.a = (maxalpha / intervals) * x;
                BlurBorder.color = BlurBorderColour;
                Debug.Log(x);
            }

            //GetComponent<Renderer>().material.color = OrigionalColour;

            for (int x = intervals; x >= 0; x--)
            {
                yield return new WaitForSeconds((time / intervals));
                BlurBorderColour.a = (maxalpha / intervals) * x;
                BlurBorder.color = BlurBorderColour;
            }
            //GetComponent<Renderer>().material.color = FlashColour;
            //PlatformGlow.SetActive(true);
        }
    }
}
