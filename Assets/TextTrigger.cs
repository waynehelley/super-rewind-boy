﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextTrigger : MonoBehaviour
{

    private GameObject CanvasText;
    public GameObject ButtonGlow1;
    public GameObject ButtonGlow2;
    public string Text;

    // Use this for initialization
    void Start()
    {

        CanvasText = GameObject.FindGameObjectWithTag("Text");

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {

            CanvasText.GetComponent<Text>().text = Text;
            CanvasText.GetComponent<Text>().enabled = true;

            if(ButtonGlow1 != null)
            {
                ButtonGlow1.GetComponent<Image>().enabled = true;
            }

            if (ButtonGlow2 != null)
            {
                ButtonGlow2.GetComponent<Image>().enabled = true;
            }

        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {

            CanvasText.GetComponent<Text>().enabled = false;
        }

        if (ButtonGlow1 != null)
        {
            ButtonGlow1.GetComponent<Image>().enabled = false;
        }

        if (ButtonGlow2 != null)
        {
            ButtonGlow2.GetComponent<Image>().enabled = false;
        }

    }

}
