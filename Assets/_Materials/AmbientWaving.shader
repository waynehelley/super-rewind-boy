﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Wayne/Ambient Waving" {

	Properties{
		_Color("Colour", Color) = (1.0, 1.0, 1.0, 1.0)
		_WaveSize("WaveSize", float) = 0.125
	_WaveLength("WaveLength", float) = 10
	_Frequency("Frequency", float) = 200
		_MinY("MinY", float) = 0
		_MaxY("MaxY", float) = 1
		_Emission("Emission", Range(0,1)) = 0

	}


		SubShader{

		Pass{

		Tags {"LightMode" = "ForwardBase"}

		CGPROGRAM

		//pragmas
#pragma vertex vert
#pragma fragment frag

		//user defined variables
		uniform float4 _Color;
	uniform float _WaveSize;
	uniform float _WaveLength;
	uniform float _Frequency;
	uniform float _MinY;
	uniform float _MaxY;
	uniform float _Emission;

	//unity defined variables
	uniform float4 _LightColor0;

	//base input structs
	struct vertexInput {
		float4 vertex : POSITION;
		float3 normal : NORMAL;
		float4 col : COLOR;
	};

	struct vertexOutput {
		float4 pos : SV_POSITION;
		float4 col : COLOR;
	};

	//vertex function
	vertexOutput vert(vertexInput v) {
		vertexOutput o;
		
		float waveAmplitude = _WaveSize * clamp((v.vertex.y - _MinY) / (_MaxY - _MinY) , 0, 1);
		

		//waving mesh		
		v.vertex.x += sin((v.vertex.y + _Time * _Frequency) / _WaveLength) * waveAmplitude;
	
		float3 normalDirection = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
		float3 lightDirection;
		float atten = 1;

		lightDirection = normalize(_WorldSpaceLightPos0.xyz);

		float3 diffuseReflection = atten * _LightColor0.xyz * max(0.0 , dot(normalDirection, lightDirection));
		float3 lightFinal = diffuseReflection + UNITY_LIGHTMODEL_AMBIENT.xyz;
		

		o.col = float4(lightFinal * _Color.rgb, 1.0) + _Emission * float4(_Color.rgb, 1.0);
		o.pos = UnityObjectToClipPos(v.vertex); //mul = multiply
		return o;
	}

	//fragment function
	float4 frag(vertexOutput i) : COLOR
	{

		return i.col;
	}


		ENDCG
	}

	}

		//fallback commented out during development
		//Fallback "Diffuse"

}
