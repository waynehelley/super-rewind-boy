﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Wayne/Tutorials/Beginner/Flat Colour " {

	Properties{
		_Colour("Colour", Color) = (1.0, 1.0, 1.0, 1.0)
	}


		SubShader{

				Pass{
				CGPROGRAM

				//pragmas
				#pragma vertex vert
				#pragma fragment frag

				//user defined variables
				uniform float4 _Colour;
				
				//base input structs
				struct vertexInput {
					float4 vertex : POSITION;
				};

				struct vertexOutput {
					float4 pos : SV_POSITION;
				};
					
				//vertex function
				vertexOutput vert(vertexInput v) {
					vertexOutput o;
					o.pos = UnityObjectToClipPos(v.vertex); //mul = multiply
					return o;
				}

				//fragment function
				float4 frag(vertexOutput i) : COLOR
				{
					return _Colour;
				}


				ENDCG
					}

				}

		//fallback commented out during development
		//Fallback "Diffuse"

	}
