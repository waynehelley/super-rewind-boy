﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {

    public GameObject PairedPortal;
    public bool Active = true;
    private Portal PairedPortalScript;

	// Use this for initialization
	void Start () {

        PairedPortalScript = PairedPortal.GetComponentInChildren<Portal>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && Active)
        {
            other.transform.position = PairedPortal.transform.position;
            PairedPortalScript.Active = false;
            StartCoroutine(PortalCoolDown());
        }            
    }

    IEnumerator PortalCoolDown()

    {
        Debug.Log("Portal Countdown Started");

        yield return new WaitForSeconds(5.0f);
        PairedPortalScript.Active = true;
        Debug.Log("Portal Countdown Run");

    }


}
