﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ActivateResumeButtons : MonoBehaviour {

    private Button ResumeButton;
    private Button LevelSelectButton;   

    // Use this for initialization
    void Start () {

        ResumeButton = GameObject.FindGameObjectWithTag("ResumeButton").GetComponent<Button>();
        LevelSelectButton = GameObject.FindGameObjectWithTag("LevelSelectButton").GetComponent<Button>();
        
        if(PlayerPrefs.HasKey("LevelReached"))
        {
            ResumeButton.interactable = true;
            LevelSelectButton.interactable = true;
        }
        else
        {
            ResumeButton.interactable = false;
            LevelSelectButton.interactable = false;
        }
                    
    }
	
}
