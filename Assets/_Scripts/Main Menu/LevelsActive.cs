﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelsActive : MonoBehaviour {

	// Use this for initialization
	void Start () {

        int LevelReached = 0;
        try
        {
            LevelReached = PlayerPrefs.GetInt("LevelReached");
        }
        catch
        {

        }

        int counter = 0;
        foreach (Transform child in transform)
        {
            if (LevelReached > counter)
            {
                child.gameObject.GetComponent<Button>().interactable = true;
            }
            else
            {
                child.gameObject.GetComponent<Button>().interactable = false;
            }

            counter++;
        }

    }

}
