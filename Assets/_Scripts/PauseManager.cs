﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PauseManager : MonoBehaviour {

    private bool paused;
    public GameObject pauseMenu;

	Canvas canvas;
    GameObject player;
    PlayerPlatformerController playerController;

	void Start()
	{
		canvas = GetComponent<Canvas>();
        paused = false;
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
            //canvas.enabled = !canvas.enabled;

            if (!paused)
            {
                Pause();
            }
            else
            {
                Unpause();
            }
		}
	}

	public void Pause()
	{
        Debug.Log("Pause");
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
        paused = true;

    }

    public void Unpause()
    {
        Debug.Log("Unpause");
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        paused = false;

    }

	public void Quit()
	{
#if UNITY_EDITOR 
		EditorApplication.isPlaying = false;
#else 
		Application.Quit();
#endif
	}
}
