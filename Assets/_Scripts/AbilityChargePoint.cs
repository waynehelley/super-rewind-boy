﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AbilityChargePoint : MonoBehaviour {

	private GameObject Player;

	public GameObject abilityPickerUI;

	public float chargeDelay;

	private float nextUse = 5;

    public string Ability;

    private GameObject AttackButtonGlow;

    void Awake()
	{
		Player = GameObject.FindGameObjectWithTag ("Player");
        AttackButtonGlow = GameObject.FindGameObjectWithTag("AttackButtonGlow");

    }

	void OnTriggerEnter2D(Collider2D other)
	{
		
		if (other.tag == "Player" && Time.time >= nextUse || Time.time < 5) {
                      
           activateStation();          
           nextUse = Time.time + chargeDelay;
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
#else
            AttackButtonGlow.GetComponent<Image>().enabled = true;
#endif

        }
	}
	
	
	void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player") {
			
			AbilityBar pstats = Player.GetComponent<AbilityBar> ();
			pstats.AbilityBarDrainStart ();			
		}
	}
	void activateStation(){

        //activate the ability picker UI and freeze time

        AbilityBar abilityBar = Player.GetComponent<AbilityBar>();
        Abilities abilities = Player.GetComponent<Abilities>();        

        abilityBar.EmptyAbilityBar();

        switch (Ability)
        {
            case "Fireball":
                //Power up Fireball ability
                abilityBar.FillAbilityBar();
                abilities.activePower = "Fireball";
                Debug.Log("Fireball Ability selected");            
                break;
            case "Flight":
                //Power up Fireball ability
                abilityBar.FillAbilityBar();
                abilities.activePower = "Flight";
                Debug.Log("Flight Ability selected");
                break;
            case "ReverseGravity":
                //Power up Fireball ability
                abilityBar.FillAbilityBar();
                abilities.activePower = "ReverseGravity";
                Debug.Log("ReverseGravity Ability selected");
                break;
            default:
                Time.timeScale = 0;                
                abilityPickerUI.SetActive(true);
                break;
        }
         		
		
		//AbilityBar pstats = Player.GetComponent<AbilityBar> ();
		
		//Debug.Log ("Made contact");
		AudioSource audio = GetComponent<AudioSource>();
		audio.Play();

	}
}