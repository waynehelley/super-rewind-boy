﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;

public class ChangeScene : MonoBehaviour {

    private int CurrentLevel;

    public void Start()
    {
        CurrentLevel = SceneManager.GetActiveScene().buildIndex;
        if (CurrentLevel > 1)
        {
            PlayerPrefs.SetInt("LevelPlayedLast", CurrentLevel);            
        }
    }
	
	public void ChangeToScene (string SceneToChangeTo) {
        Debug.Log("Change to scene");
        //if (SceneToChangeTo == "Main Menu")
        //{
        //    ChangeLevelWithoutFade(SceneToChangeTo);
        //}
        //else
        //{

        //timeScale must be set to 1 otherwise fadecoroutine will never move.
        Time.timeScale = 1;

        float fadeTime = GameObject.Find("_Manager").GetComponent<Fading>().BeginFade(1);
            StartCoroutine(FadePause(SceneToChangeTo, fadeTime));
        //}

        
    }

    public void ChangeToLevelPlayedLast()
    {
        ChangeToScene(PlayerPrefs.GetInt("LevelPlayedLast").ToString());
    }

    private void ChangeLevelWithoutFade(string SceneToChangeTo)
    {
        if (SceneToChangeTo == "Next")
        {
            try { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); }
            catch { SceneManager.LoadScene("Main Menu"); }

        }
        else
        {
            //find out if SceneToChangeTo is numeric or text
            bool isNumeric = Regex.IsMatch(SceneToChangeTo, @"^\d+$");

            if (!isNumeric)
            {
                SceneManager.LoadScene(SceneToChangeTo);
            }
            else
            {
                SceneManager.LoadScene(int.Parse(SceneToChangeTo));
            }

        }
    }

    IEnumerator FadePause(string SceneToChangeTo, float fadeTime)
    {
        Debug.Log("Fade Started");
        yield return new WaitForSeconds(fadeTime);
        Debug.Log("Countdown Done");

        ChangeLevelWithoutFade(SceneToChangeTo);

    }
    
}
