﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;

namespace UnityStandardAssets._2D
{
    public class Restarter : MonoBehaviour
    {

        void OnTriggerEnter2D(Collider2D other)
        {

            ///This code also exists in GumbaEnemy
            ///It should be a subroutine in DeathScript

            if (other.tag == "Player")               
                {                
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                }

        }

    }


}
