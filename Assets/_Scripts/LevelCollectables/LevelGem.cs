﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelGem : MonoBehaviour {

	private GameObject CoinCollectNoise;
    private AudioSource noise;

    public LevelDoor Door;
    private GameObject LevelGem1;
    private GameObject LevelGem2;
    private GameObject LevelGem3;    

    void Awake()
    {
        Door = GameObject.FindObjectOfType<LevelDoor>();
        LevelGem1 = GameObject.FindGameObjectWithTag("LevelGem1");
        LevelGem2 = GameObject.FindGameObjectWithTag("LevelGem2");
        LevelGem3 = GameObject.FindGameObjectWithTag("LevelGem3");

        CoinCollectNoise = GameObject.FindGameObjectWithTag("CoinCollectNoise");
        noise = CoinCollectNoise.GetComponent<AudioSource>();

    }

        void OnTriggerEnter2D(Collider2D other)
	{
		
		if (other.tag == "Player") 
		{
			
			noise.Play();
            
            switch (Door.collectedCount)
            {
                case 0 :
                    LevelGem1.GetComponent<Image>().enabled = true;                    
                    break;
                case 1:
                    LevelGem2.GetComponent<Image>().enabled = true;
                    break;
                case 2:
                    LevelGem3.GetComponent<Image>().enabled = true;
                    break;
            }

            Door.AddOne();         

			Destroy (gameObject);
		}
		
	}
}
