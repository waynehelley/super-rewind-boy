﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelDoor : MonoBehaviour {
	public int collectedCount =0;
	public bool isOpen = false;
    public string SceneToChangeTo;

    public int totalCollectableCount=3;
	Animator animator;

	void Awake(){
		animator = GetComponent<Animator>();
	}	
	
	// Update is called once per frame
	void Update () {
		if(collectedCount == totalCollectableCount && !isOpen){
			isOpen = true;
			animator.SetBool("isOpen",true);
		}
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Collision");
        //Add logic for if all gems have been collected and door is open
        if (other.tag == "Player" && isOpen == true)        {
            Debug.Log(isOpen);
            ChangeScene pstats = GetComponent<ChangeScene>();

            int LevelReached = 0;
            try
            {
                LevelReached = PlayerPrefs.GetInt("LevelReached");
                    }
            catch
            {

            }

            int CurrentLevel = SceneManager.GetActiveScene().buildIndex;

            if (CurrentLevel >= LevelReached)
                {
                PlayerPrefs.SetInt("LevelReached", CurrentLevel + 1);
            }
             
            pstats.ChangeToScene(SceneToChangeTo);

        }        

    }

    public void AddOne()
    {
        collectedCount++;
    }

}
