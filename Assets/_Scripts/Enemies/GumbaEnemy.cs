﻿using UnityEngine;
using Chronos;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;

public class GumbaEnemy : BaseBehaviour {

	public float velocity = 1f;
	private RigidbodyTimeline2D m_Rigidbody2D;

	public Transform sightStart;
	public Transform sightEnd;

	public LayerMask detectWhat;

	public Transform weakness;

	public bool colliding;

    private TimeControl timeControl;
    private GameObject TimeButtonGlow;
    private GameObject MainCamera;
    
    Animator anim;

    void Awake ()
    {
        timeControl = FindObjectOfType<TimeControl>();
        TimeButtonGlow = GameObject.FindGameObjectWithTag("TimeButtonGlow");
        MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

	// Use this for initialization
	void Start () {


        //m_Rigidbody2D = GetComponent<Rigidbody2D>();
                      

        m_Rigidbody2D = time.rigidbody2D;

            anim = GetComponent<Animator> ();
              


    }
	
	// Update is called once per frame
	void Update () {

        if (Time.timeScale > 0) // Move only when time is going forward
        {
            m_Rigidbody2D.velocity = new Vector2(velocity,m_Rigidbody2D.velocity.y);
        }

		colliding = Physics2D.Linecast (sightStart.position, sightEnd.position, detectWhat);

		if (colliding) {
			transform.localScale=new Vector2 (transform.localScale.x * -1, transform.localScale.y);
			velocity*=-1;

		}

		//if health is depleted, die
		if(GetComponent<EnemyDestructibleHealth>() != null)
		{
			if(GetComponent<EnemyDestructibleHealth>().health <= 0)
			{
				EnemyDies();
			}
		}

	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.magenta;
		Gizmos.DrawLine (sightStart.position, sightEnd.position);
	}

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
        {

            float height = col.contacts[0].point.y - weakness.position.y;

            if (false) //(height > 0)
            {
                EnemyDies();
                col.rigidbody.AddForce(new Vector2(0, 200));
            }
            else
            {

                ///This was all copied and pasted over from 'Restarter'
                ///It should be a subroutine in DeathScript

                if (!DarkMode.darkmode)
                {
                    //DeathScript pstats = Player.GetComponent<DeathScript>();
                    timeControl.TimePaused = true;
                    timeControl.t = 0;
                    Debug.Log("Player Death - Collision Detected");
                    TimeButtonGlow.GetComponent<Image>().enabled = true;
                    MainCamera.GetComponent<Grayscale>().enabled = true;

                }
                else
                {

                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

                }
            }

        }


    }

    void EnemyDies()
	{
		anim.SetBool ("Stomped", true);
		Destroy (this.gameObject, 0.5f );
		gameObject.tag = "Neutralized";
		velocity = 0;

		
	}
    
}
