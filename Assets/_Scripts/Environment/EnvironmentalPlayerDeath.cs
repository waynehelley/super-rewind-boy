using System;
using System.Collections;
using UnityEngine;

public class EnvironmentalPlayerDeath : MonoBehaviour
{

    Death deathScript;

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Player")
        {
            deathScript = other.GetComponent<Death>();
            deathScript.PlayerDeath();
        }

    }

}


