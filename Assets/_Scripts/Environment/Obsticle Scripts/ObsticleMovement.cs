﻿using UnityEngine;
using System.Collections;
using Chronos;

public class ObsticleMovement : MonoBehaviour
{
    [Header("Obsticle Movment Variables")]
    public Transform[] Waypoints;
    public float speed;
    private int Marker = 0;

    private ObsticleRotation reverseRotation;

    private Clock clock;

    void Start()
    {
        reverseRotation = this.GetComponent<ObsticleRotation>();
        clock = Timekeeper.instance.Clock("Enemies");
    }

    // Update is called once per frame
    void Update()
    {
        if (clock.localTimeScale == 1)
        {
            MoveObsticle();
        }
    }

    void MoveObsticle()
    {

        transform.position = Vector3.MoveTowards(transform.position, Waypoints[Marker].transform.position, speed * Time.deltaTime);

        if (transform.position == Waypoints[Marker].transform.position)
        {
            Marker++;
            reverseRotation.ReverseRotation = false;
        }
        if (Marker == Waypoints.Length)
        {
            Marker = 0;
            reverseRotation.ReverseRotation = true;
        }
    }
}
