﻿using UnityEngine;
using System.Collections;
using Chronos;

public class MovingPlatform : MonoBehaviour {
    		
	public int Marker = 0;
    
	public int speed =4;    
    public Transform[] Waypoints;

    private Clock clock;
    
    void Start () 
	{        
        clock = Timekeeper.instance.Clock("Enemies");
    }

	// Update is called once per frame
	void Update () {

        if (clock.localTimeScale > 0) //only move platfrom when time is moving forwards
        {
            transform.position = Vector3.MoveTowards(transform.position, Waypoints[Marker].transform.position, speed * Time.deltaTime);

            if (transform.position == Waypoints[Marker].transform.position)
            {
                Marker++;
            }
            if (Marker == Waypoints.Length)
            {
                Marker = 0;
            }
        }        

	}
    
}
