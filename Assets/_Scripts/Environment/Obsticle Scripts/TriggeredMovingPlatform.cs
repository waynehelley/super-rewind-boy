﻿using UnityEngine;
using System.Collections;
using Chronos;

public class TriggeredMovingPlatform : MonoBehaviour
{

    private GameObject player;

    public int Marker = 0;
    public int WayPointLength;
    public int speed = 4;
    public Transform[] Waypoints;
    public GameObject Trigger;
    public bool TimeControlled = true;
    public Color FlashColour;
    private Color OrigionalColour;   
    private SpriteRenderer BlurBorder;
    private Color BlurBorderColour;
    public bool ContinuouslyMoving = false;
    private bool stop = false;
    

    void Start()
    {
              

        player = GameObject.Find("Player"); //Player's Character
        WayPointLength = Waypoints.Length;
        GetComponent<Timeline>().enabled = TimeControlled;
        Trigger.GetComponent<Timeline>().enabled = TimeControlled;
              

    }

    // Update is called once per frame
    void Update()
    {
        if (!stop)
        {
            transform.position = Vector3.MoveTowards(transform.position, Waypoints[Marker].transform.position, speed * Time.deltaTime);
        }

        if (!ContinuouslyMoving)
        {

            if (transform.position == Waypoints[Marker].transform.position & Trigger.GetComponent<SwitchScript>().down == true)
            {
                if (Marker < WayPointLength - 1)
                {
                    Marker++;
                }
            }
            if (Marker == Waypoints.Length - 1 & Trigger.GetComponent<SwitchScript>().down == false)
            {
                Marker = 0;
            }
        }
        else
        {
            if (Trigger.GetComponent<SwitchScript>().down == true) //& clock.localTimeScale > 0) //only move platfrom when time is moving forwards
            {
                if (transform.position == Waypoints[Marker].transform.position)
                {
                    Marker++;
                }
                if (Marker == Waypoints.Length)
                {
                    Marker = 0;
                }
                stop = false;
            }
            else
            {
                stop = true;
            }
            }
        }

    }
    
    

