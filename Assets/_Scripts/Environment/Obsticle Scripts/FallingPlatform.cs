﻿using UnityEngine;
using System.Collections;

public class FallingPlatform : MonoBehaviour {

    public bool falling = false;

    private Rigidbody2D Rigidbody;

    void Start()
    {

        Rigidbody = GetComponent<Rigidbody2D>();

    }

    void Update()
    {
        if (falling)
        { Rigidbody.constraints &= ~RigidbodyConstraints2D.FreezePositionY;}
        else
        { Rigidbody.constraints = RigidbodyConstraints2D.FreezeAll; }

    }

    void OnCollisionEnter2D(Collision2D col)
    {

        Debug.Log("FallingPlatfrom Collision");

        if (col.gameObject.tag == "Player")
        {
            Debug.Log("FallingPlatfrom Collision=Player");
            StartCoroutine(PlatformFallCountDown());
        }


    }

    IEnumerator PlatformFallCountDown()

    {
        Debug.Log("FallingPlatfrom Countdown Started");
        
        yield return new WaitForSeconds(0.5f);
        falling = true;           
        Debug.Log("FallingPlatfrom Countdown Run");

    }

}

