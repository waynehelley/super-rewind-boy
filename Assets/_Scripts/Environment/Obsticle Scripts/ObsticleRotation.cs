﻿using UnityEngine;
using System.Collections;
using Chronos;

public class ObsticleRotation : MonoBehaviour
{

    [Header("Rotation Values")]
    public float rotationSpeed;

    private bool reverseRotation;
    public bool ReverseRotation        
    {
        get { return reverseRotation; }
        set { reverseRotation = value; }
    }
    private Clock clock;

    void Start(){
        clock = Timekeeper.instance.Clock("Enemies");
        }

	// Update is called once per frame
	void Update ()
    {
        if (clock.localTimeScale == 1)
        {
            RotateObject();
        }
    }

    public void RotateObject()
    {
        if (reverseRotation == true)
        {
            transform.Rotate(Vector3.up, -rotationSpeed * Time.deltaTime);
        }
        else
        {
            transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
        }
        
    }


}
