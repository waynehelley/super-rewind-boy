﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof(LunaCharacterController))]
    public class PlatformChild : MonoBehaviour {

        private GameObject player;
        private LunaCharacterController m_Character;

        void Start()
        {
            player = GameObject.Find("Player"); //Player's Character
            m_Character = player.GetComponent<LunaCharacterController>();

        }

        //If character collides with the platform, make it its child.
        void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll.gameObject.tag == "Player")
            {
                MakeChild();
                m_Character.onSwingingPlatfrom = true;
            }
        }
        //Once it leaves the platform, become a normal object again.
        void OnCollisionExit2D(Collision2D coll)
        {
            if (coll.gameObject.tag == "Player")
            {
                ReleaseChild();
                m_Character.onSwingingPlatfrom = false;
            }
        }

        void MakeChild()
        {
            player.transform.parent = transform;
        }

        void ReleaseChild()
        {
            player.transform.parent = null;
        }
    }
}
