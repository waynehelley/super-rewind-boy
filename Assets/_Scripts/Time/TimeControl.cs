﻿using UnityEngine;
using Chronos;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets._2D;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;

public class TimeControl : MonoBehaviour
{

    private MonoBehaviour lunaUserControl; //eventually change to type PlayerPlatformerController
    private Rigidbody2D lunaRigidBody;
    private TimeControl timeControl;
    public bool TimePaused;  
    public float t=1;
    public GameObject TimeButtonGlow;
    private GameObject MainCamera;
    private GameObject CanvasText;
    private GameObject player;
    private bool wasTimeRewindingOnLastFrame;

    void Awake()
    {
        try
        {
            lunaUserControl = FindObjectOfType<PlayerPlatformerController>();
        }
        catch
        {
            lunaUserControl = FindObjectOfType<LunaUserControl>();
        }
        lunaRigidBody = FindObjectOfType<Rigidbody2D>();
        TimeButtonGlow = GameObject.FindGameObjectWithTag("TimeButtonGlow");
        MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    void Start()
    {

        player = GameObject.Find("Player"); //Player's Character

        if (SceneManager.GetActiveScene().name == "Tutorial 01")
        {
            CanvasText = GameObject.FindGameObjectWithTag("Text");
        }

        lunaRigidBody = player.GetComponent<Rigidbody2D>();

    }

    void Update()
    {   // Get the Enemies global clock       
        Clock clock = Timekeeper.instance.Clock("Enemies");

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
        //if (!m_Rewind)
        //{
        // Read the jump input in Update so button presses aren't missed.        
        if (CrossPlatformInputManager.GetButton("xbox button x"))
        {
            clock.localTimeScale = -2; // Rewind
            lunaUserControl.enabled = false;
            TimePaused = false;
            Debug.Log("Rewind button press");

            if (wasTimeRewindingOnLastFrame == false)
            {
                timeEffectsOn();
            }

            wasTimeRewindingOnLastFrame = true;

        }

        else if (TimePaused == true)
        {
            //add if function to ensure time is not paused due to death
            clock.localTimeScale = 0; // Normal}
            lunaUserControl.enabled = false;
            lunaRigidBody.constraints = RigidbodyConstraints2D.FreezeAll;

            if (wasTimeRewindingOnLastFrame == false)
            {
                timeEffectsOn();
            }

            wasTimeRewindingOnLastFrame = true;

        }

        else if (CrossPlatformInputManager.GetButton("xbox button x") == false) 
        {
            //add if function to ensure time is not paused due to death
            clock.localTimeScale = 1; // Normal}
            lunaUserControl.enabled = true;
            //TimeScale = 1;

            if (wasTimeRewindingOnLastFrame == false)
            {
                timeEffectsOff();
            }

            wasTimeRewindingOnLastFrame = false;

        }
             
        
#else
	clock.localTimeScale = t;
#endif
}
		public void timebuttonpress()
		{
			t = -2;        
        TimeButtonGlow.GetComponent<Image>().enabled = false;

        timeEffectsOn();

        if (SceneManager.GetActiveScene().name == "Tutorial 01")
        {
            if (CanvasText.GetComponent<Text>().text == "Hold the highlighted button to rewind time")
            {
                CanvasText.GetComponent<Text>().enabled = false;
            }
        }

    }

		public void timebuttonrelease()
		{
			t = 1;
        timeEffectsOff();

    }


    public void timeEffectsOn()
    {

        MainCamera.GetComponent<MotionBlur>().enabled = true;
        MainCamera.GetComponent<Grayscale>().enabled = true;

        //disable collider on Player while rewinding so that Player doesn't move objects (crates, etc.)
        player.GetComponent<BoxCollider2D>().enabled = false;
        lunaRigidBody.constraints = RigidbodyConstraints2D.FreezeAll;   

    }

    public void timeEffectsOff()
    {

        //reactivate players collider
        player.GetComponent<BoxCollider2D>().enabled = true;
        lunaRigidBody.constraints = RigidbodyConstraints2D.FreezeRotation;

        MainCamera.GetComponent<MotionBlur>().enabled = false;
        MainCamera.GetComponent<Grayscale>().enabled = false;

    }

        //// Change its time scale on key press
        //if (Input.GetKeyDown(KeyCode.Alpha1))
        //{
        //    clock.localTimeScale = -1; // Rewind
        //}
        //else if (Input.GetKeyDown(KeyCode.Alpha2))
        //{
        //    clock.localTimeScale = 0; // Pause
        //}
        //else if (Input.GetKeyDown(KeyCode.Alpha3))
        //{
        //    clock.localTimeScale = 0.5f; // Slow
        //}
        //else if (Input.GetKeyDown(KeyCode.Alpha4))
        //{
        //    clock.localTimeScale = 1; // Normal
        //}
        //else if (Input.GetKeyDown(KeyCode.Alpha5))
        //{
        //    clock.localTimeScale = 2; // Accelerate
        //}
    
    
}

