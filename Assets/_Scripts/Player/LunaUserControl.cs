using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
	[RequireComponent(typeof (LunaCharacterController))]
	public class LunaUserControl : MonoBehaviour
    {
		private LunaCharacterController m_Character;
        private bool m_Jump;
		public bool m_Ability;
		public float h;
        public bool enablegamepads;
        public bool abilityButtonHeld;
        public bool jumpButtonHeld;
        public bool sprint;
        private float lastTime;

        private void Awake()
        {

            h = 0;
            m_Character = GetComponent<LunaCharacterController>();
        }


        private void Update()
        {

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
            
            if (!m_Jump)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("xbox button a");
		     }

			if (!m_Ability)
			{
				// Read the jump input in Update so button presses aren't missed.
				m_Ability = CrossPlatformInputManager.GetButtonDown("xbox button b");
			}

            if (CrossPlatformInputManager.GetButton("xbox button b"))  //If button is held down
            {
                abilityButtonHeld = true;
            }
            else
            {
                abilityButtonHeld = false;
            }

            if (CrossPlatformInputManager.GetButton("xbox button a"))  //If button is held down
            {
                jumpButtonHeld = true;
            }
            else
            {
                jumpButtonHeld = false;
            }

#endif

        }




        private void FixedUpdate()
        {
            // Read the inputs.
            
            //Works for keyboards and joysticks
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
            if (enablegamepads)
            {
                h = CrossPlatformInputManager.GetAxis("HorizontalGamePad") + CrossPlatformInputManager.GetAxis("Horizontal");
                sprint = CrossPlatformInputManager.GetButton("xbox button rb");
            }
            else
            {
                h = CrossPlatformInputManager.GetAxis("Horizontal");
                bool crouch = Input.GetKey(KeyCode.LeftControl);
            }      

            // Pass all parameters to the character control script.
            m_Character.Move(h, sprint);
			m_Character.Jump (m_Jump, jumpButtonHeld);
			m_Jump = false;
			m_Character.Ability (m_Ability, abilityButtonHeld);
			m_Ability = false;
#else
            m_Character.Move (h, sprint);
            m_Character.Jump(m_Jump, jumpButtonHeld);
            m_Jump = false;
#endif


        }

		public void leftbuttonpress()
		{
			h = -1;
            sprintLogic();
        }

		public void rightbuttonpress()
		{
			h = 1;
            sprintLogic();
        }

		public void buttonrelease()
		{
			h = 0;
		}

        private void sprintLogic()
        {
            if (Time.time - lastTime < 0.2f)
            {
                lastTime = Time.time;
                // turn on running
                sprint = true;
            }
            else
            {
                lastTime = Time.time;
                // turn on (or switch to) walking
                sprint = false;
            }
        }

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT

#else

        public void jumpButtonDown()
        {
           m_Jump = true;
            jumpButtonHeld = true;
        }

        public void jumpButtonUp()
        {
            jumpButtonHeld = false;
        }


#endif

    }
}
