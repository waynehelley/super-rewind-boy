﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AbilityBar : MonoBehaviour {
	
	public GUIStyle BlackBar;
	public GUIStyle NoAbility;
	public GUIStyle TimeSlow;
	public GUIStyle TextField;
	
	public bool AbilityEnabled=false;
	public bool AbilityDraining=false;
	public float MaxAbilityCharge = 200;
	public float CurrentAbilityCharge = 0;
	public float abilityCost;
    public GameObject AttackButtonGlow;

    public GUIStyle currentAbility;

    private GameObject AbilityCountdownNoise;
    private AudioSource noise;
    public bool AbilityCountdownNoisePlayed;

    private GameObject AbilityBarEmptyNoise;
    private AudioSource noiseEmpty;
    private bool AbilityBarEmptyNoisePlayed;

    void Start(){
		currentAbility = NoAbility;
	}

    void Awake()
    {

        AbilityCountdownNoise = GameObject.FindGameObjectWithTag("AbilityCountdownNoise");
        noise = AbilityCountdownNoise.GetComponent<AudioSource>();
        AbilityCountdownNoisePlayed = false;

        AbilityBarEmptyNoise = GameObject.FindGameObjectWithTag("AbilityBarEmptyNoise");
        noiseEmpty = AbilityBarEmptyNoise.GetComponent<AudioSource>();
        AbilityBarEmptyNoisePlayed = true;

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
#else
        AttackButtonGlow = GameObject.FindGameObjectWithTag("AttackButtonGlow");
#endif
    }

    void Update(){
		
		if (CurrentAbilityCharge <= 0) {
			AbilityEnabled = false;
			CurrentAbilityCharge=0;
            noise.Stop();

            if (AbilityBarEmptyNoisePlayed==false)
            {
                noiseEmpty.Play();
                AbilityBarEmptyNoisePlayed = true;
            }
                       

            //see if this can be optimised
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
#else
            AttackButtonGlow.GetComponent<Image>().enabled = false;
#endif
        }
		
		if (AbilityDraining==true){
			//Debug.Log("AbilityEnabled");
			CurrentAbilityCharge-=((Time.deltaTime*8)/Time.timeScale + (abilityCost/7));

            if(CurrentAbilityCharge<80)
            {

                //playability Countdown noise
                if (AbilityCountdownNoisePlayed == false)
                {
                    Debug.Log("AbilityCountdown Noise played");
                    noise.Play();
                    AbilityCountdownNoisePlayed = true;
                }
            }
                        

        }
		
		
		
		
		
		
	}
	
	void OnGUI()
	{
		//if (!Death) {  //If NOT death
		
		
		//Experience bar
		GUI.Box (new Rect (10, 10, 200, 10), "", BlackBar);  //BlackBar bar hehind the text
		GUI.Box (new Rect (10, 10, CurrentAbilityCharge, 10), "", currentAbility);
		//GUI.Box (new Rect (20, 5, 200, 25), "XP: " + CurrentAbilityChange + "/" + MaxAbilityCharge, TextField);
		
		
		
		
	}
	
	public void FillAbilityBar()
	{
		Debug.Log("Ability Bar Fill");
		CurrentAbilityCharge = MaxAbilityCharge;
		AbilityEnabled = true;
		AbilityDraining = false;
        AbilityCountdownNoisePlayed = false;
        AbilityBarEmptyNoisePlayed = false;
        noise.Stop();

    }
	
	public void AbilityBarDrainStart()
	{
		//Debug.Log("Ability Bar Fill");
		AbilityDraining = true;
    }
	
	
	
	public void EmptyAbilityBar()
	{
		CurrentAbilityCharge = 0;
		AbilityEnabled = false;
		AbilityDraining = false;
	}
	
}