﻿using UnityEngine;
using System.Collections;

public class GravityReverseObject : MonoBehaviour {

    private Rigidbody2D m_Rigidbody2D;
    private TimeImmune timeImmuneScript;
    private bool TimeControled;
    private float gravityDown = 1;
    private float gravityUp = -0.5f;

    public void Awake()
    {
        timeImmuneScript = GetComponent<TimeImmune>();
        TimeControled = timeImmuneScript.TimeControlled;
        
        if(TimeControled)
        {
            // for some reason, gravity functions differently when controlled by Chronos
            gravityDown = 0.05f;
            gravityUp = -1.50f;
        }

        m_Rigidbody2D = GetComponent<Rigidbody2D>();
        m_Rigidbody2D.gravityScale = gravityDown;

    }
    
    public void ReverseGravityOnObject()
    {        

        m_Rigidbody2D.gravityScale = gravityUp;

    }

    public void NormalGravityOnObject()
    {

        
        m_Rigidbody2D.gravityScale = gravityDown;

    }

}
