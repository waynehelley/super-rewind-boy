﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Death : MonoBehaviour {

    private GameObject Player;
    private TimeControl timeControl;
    private GameObject TimeButtonGlow;
    private GameObject AttackButtonGlow;
    private GameObject MainCamera;
    private GameObject CanvasText;

    void Awake()
    {

        timeControl = FindObjectOfType<TimeControl>();

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
#else
        TimeButtonGlow = GameObject.FindGameObjectWithTag("TimeButtonGlow");
        AttackButtonGlow = GameObject.FindGameObjectWithTag("AttackButtonGlow");
#endif
        MainCamera = GameObject.FindGameObjectWithTag("MainCamera");

    }

    void Start()
    {
        //These functions have been moved out of Awake and into Start to that they trigger later
        //incase other Scripts are trying to declare these objects
        //decleration will fail if objects aren't active in scene

#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
#else
        TimeButtonGlow.GetComponent<Image>().enabled = false;
        AttackButtonGlow.GetComponent<Image>().enabled = false;
#endif

        if (SceneManager.GetActiveScene().name == "Tutorial 01")
        {
            CanvasText = GameObject.FindGameObjectWithTag("Text");
        }

    }

    public void PlayerDeath()

    {

        if (!DarkMode.darkmode)
        {
            //DeathScript pstats = Player.GetComponent<DeathScript> ();
            timeControl.TimePaused = true;
            timeControl.t = 0;

            timeControl.timeEffectsOn();

            Debug.Log("Player Death - Collision Detected");
#if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
#else
            TimeButtonGlow.GetComponent<Image>().enabled = true;
#endif
            MainCamera.GetComponent<Grayscale>().enabled = true;
            if (SceneManager.GetActiveScene().name == "Tutorial 01")
            {
                CanvasText.GetComponent<Text>().text = "Hold the highlighted button to rewind time";
                CanvasText.GetComponent<Text>().enabled = true;

            }
        }
        else
        {

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        }

    }

}
