﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class CyclePositiveNegative : MonoBehaviour{

    private Clock clock;
    private float startTime = 0;

    void Start () {

        
        
    }
	
    public float ReturnCyclingValue(float cycleTime)
    {

        //maybe this shouldnt be here, put in PPC?
        clock = Timekeeper.instance.Clock("Enemies");

        float timePassed = clock.time;

        //this shoould generate a value between 0 and 1
        float cyclePos = (timePassed % cycleTime) / cycleTime;
        
        if (cyclePos < 0.25f)
        { return cyclePos * 4.0f; }
        else if (cyclePos < 0.5f)
        { return 1f - (cyclePos - 0.25f) * 4.0f; }
        else if (cyclePos < 0.75f)
        { return (cyclePos - 0.5f) * -4.0f; }
        else if (cyclePos < 1)
        { return -1  - (cyclePos - 0.75f) * -4.0f; }
        else
        {
            throw new System.ArgumentException("cyclePos is greater than 0");
        }
    }



    }


