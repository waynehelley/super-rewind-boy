using System;
using UnityEngine;
using Chronos;

namespace UnityStandardAssets._2D
{
    public class LunaCharacterController : BaseBehaviour
    {
		public bool slowTime = false;
        public bool onSwingingPlatfrom = false;

        private float m_MaxSpeed = 4.5f;                    // The fastest the player can travel in the x axis.
        private float m_SprintMultiplier = 1.5f;           
        private float m_JumpForce = 300f;                  // Amount of force added when the player jumps.
        private float m_JumpAssist = 35f;                  // Amount of force added when the player jumps.
        private float m_DoubleJumpForce = 400f;                  // Amount of force added when the player jumps.
        //[Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
		//[Range(1, 2)] [SerializeField] private float m_RunSpeed = 1.36f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
		[SerializeField] private bool m_AirControl = false;                 // Whether or not a player can steer while jumping;
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

		[SerializeField] private float m_AirMobilityFactor = 0.8f;          // Mobility loss while in air

        private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
        const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
        private bool m_Grounded;            // Whether or not the player is grounded.
        private Transform m_CeilingCheck;   // A position marking where to check for ceilings
        const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
        private Animator m_Anim;            // Reference to the player's animator component.
        private RigidbodyTimeline2D m_Rigidbody2D;
        public bool m_FacingRight = true;  // For determining which way the player is currently facing.
		private bool m_doubleJumped = false;
		private bool m_jumped = false;
		public float JetPackSpeed = 0.1f;
        public AbilityBar abilityBar;
        public float flightdraincounter =0;

        public bool Flight = false;        

        public string activePower;

        private Abilities pstats;
        private bool wasAbilityActiveOnLastFrame;

        private bool mobileAbilityButtonDown;
        private bool mobileAbilityButtonHeld;

        public float jumpForce;
        private float jumpTime = 0.15f;
        private float jumpTimeCounter;
        public bool stoppedJumping;

        private void Awake()
        {
            // Setting up references.
            m_GroundCheck = transform.Find("GroundCheck");
            m_CeilingCheck = transform.Find("CeilingCheck");
            m_Anim = GetComponentInChildren<Animator>();    //GetComponent<Animator>();
            m_Rigidbody2D = time.rigidbody2D;
            //m_Rigidbody2D = GetComponent<Rigidbody2D>();
            abilityBar = GetComponent<AbilityBar>();
            pstats = GetComponent<Abilities>();
        }

        void Start()
        {
            //sets the jumpCounter to whatever we set our jumptime to in the editor
            jumpTimeCounter = jumpTime;
        }

        private void Update()
        {
            #if UNITY_ANDROID || UNITY_IPHONE || UNITY_BLACKBERRY || UNITY_WINRT
                Ability(mobileAbilityButtonDown, mobileAbilityButtonHeld);
                mobileAbilityButtonDown = false;
            #endif                
        }

        private void FixedUpdate()
        {
            m_Grounded = false;


            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject){
                    m_Grounded = true;
					if(m_doubleJumped){
						m_Anim.SetBool("DoubleJumped", false);
						m_Anim.SetBool("HasDoubleJumped", false);
					}
					m_doubleJumped = false;


				}
            }
            m_Anim.SetBool("Ground", m_Grounded);
			m_Anim.SetBool("HasDoubleJumped", m_doubleJumped);
            // Set the vertical animation
            m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);
			m_jumped = !m_Grounded;

            if(m_Grounded)
            {
                //sets the jumpCounter to whatever we set our jumptime to in the editor
                jumpTimeCounter = jumpTime;
            }

            m_Anim.SetBool("Jumped", m_jumped);
        }
        
        public void Move(float move, bool run) // bool jump)
        {
            // If crouching, check to see if the character can stand up
           // if (!crouch && m_Anim.GetBool("Crouch"))
           // {
                // If the character has a ceiling preventing them from standing up, keep them crouching
               // if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
               // {
                //    crouch = true;
               // }
           // }

            // Set whether or not the character is crouching in the animator
            //m_Anim.SetBool("Crouch", crouch);

			// Set whether or not the running is crouching in the animator
			m_Anim.SetBool("Run", run);

            //only control the player if grounded or airControl is turned on
            if (m_Grounded || m_AirControl)
            {
                // Reduce the speed if crouching by the crouchSpeed multiplier
                //move = (crouch ? move*m_CrouchSpeed : move);

				// Increase the speed if crouching by the crouchSpeed multiplier
			move = (run ? move*m_SprintMultiplier : move);

                // The Speed animator parameter is set to the absolute value of the horizontal input.
                m_Anim.SetFloat("Speed", Mathf.Abs(move));

                // Move the character

				if (m_Grounded)
				{
                    //Without Air Mobility Factor
                    //if (time.timeScale > 0) //Move only when time is going forward
                    if (!onSwingingPlatfrom | move != 0)
                    {
                        { m_Rigidbody2D.velocity = new Vector2((move * m_MaxSpeed) / Time.timeScale, m_Rigidbody2D.velocity.y); }
                    }

                }
				else
				{
                    //With Air Mobility Factor to reduce player control/speed in air
                    //if (time.timeScale > 0) //Move only when time is going forward
                    { m_Rigidbody2D.velocity = new Vector2((move * m_MaxSpeed * m_AirMobilityFactor) / Time.timeScale, m_Rigidbody2D.velocity.y); }
				}

                // If the input is moving the player right and the player is facing left...
                if (move > 0 && !m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
                    // Otherwise if the input is moving the player left and the player is facing right...
                else if (move < 0 && m_FacingRight)
                {
                    // ... flip the player.
                    Flip();


                }
            }
            
        }


        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

		public void Jump(bool jump, bool jumpButtonHeld)
		{
                        
			// If the player should jump...
			if (m_Grounded && jump && m_Anim.GetBool("Ground"))
			{
				// Add a vertical force to the player.
				m_Grounded = false;
				m_Anim.SetBool("Jumped", true);
				m_Anim.SetBool("Ground", false);
                m_Rigidbody2D.velocity = new Vector3(m_Rigidbody2D.velocity.x, 0);
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
				AudioSource audio = GetComponent<AudioSource>();
				audio.Play();
                stoppedJumping = false;
            }

            //if you keep holding down the jump button...
            else if (jumpButtonHeld && !stoppedJumping)
            {
                //and your counter hasn't reached zero...
                if (jumpTimeCounter > 0)
                {
                    //keep jumping!                    
                    m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpAssist));
                    jumpTimeCounter -= Time.deltaTime;
                }
            }

            //if the player should double jump
            else if (m_jumped && jump && !m_doubleJumped)
            {
                m_doubleJumped = true;
                m_Rigidbody2D.velocity = new Vector3(m_Rigidbody2D.velocity.x, 0);
                m_Rigidbody2D.AddForce(new Vector2(0f, m_DoubleJumpForce));
                m_Anim.SetBool("DoubleJumped", true);
                AudioSource audio = GetComponent<AudioSource>();
                audio.Play();
            }

            //if you stop holding down the mouse button...
            else //would be more efficent to look at when jump button is released
            {
                //stop jumping and set your counter to zero.  The timer will reset once we touch the ground again in the update function.
                //jumpTimeCounter = 0;
                stoppedJumping = true;
            }


        }

        //Version of input with single variable is required for mobile button
        public void Ability(bool ability)
        {

            mobileAbilityButtonDown = ability;
            mobileAbilityButtonHeld = ability;

        }

		public void Ability(bool ability, bool abilityheld)
		{
			// If the player should use ability...
			if (ability && abilityBar.AbilityEnabled)
			{
                				
				// This shoots a fireball.  We need to change 
				
                switch (pstats.activePower)
                {
                    case "Fireball":
                        //Power up Fireball ability
                        pstats.ShootFireball();
                        break;
                    case "Flight":
                        m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, JetPackSpeed);
                        abilityBarDrain(2);
                        break;
                    case "ReverseGravity":
                        pstats.ReverseGravity();
                        abilityBarDrain(0.5f);
                        break;
                    default:
                        throw new Exception("No pstats.activePower selected");                        
                }

                wasAbilityActiveOnLastFrame = true;

            }
            else if (abilityheld && abilityBar.AbilityEnabled)
            {
                switch (pstats.activePower)
                {
                    case "Fireball":                        
                        break;
                    case "Flight":
                        m_Rigidbody2D.velocity = new Vector2(m_Rigidbody2D.velocity.x, JetPackSpeed);
                        abilityBarDrain(2);
                        break;
                    case "ReverseGravity":                        
                        abilityBarDrain(0.5f);
                        break;                    
                }
            }

            else
            {

                if(abilityheld==false && wasAbilityActiveOnLastFrame == true)
                {

                    switch (pstats.activePower)
                    {                        
                        case "Flight":
                            Flight = false;
                            break;
                        case "ReverseGravity":
                            pstats.ResetGravity();
                            break;                      
                    }

                    wasAbilityActiveOnLastFrame = false;

                }
                                
            }
			
		}
        
        private void abilityBarDrain(float drainValue)
        {           
            
                abilityBar.CurrentAbilityCharge -= drainValue;
                       
        }

    }
}
