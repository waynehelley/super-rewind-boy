using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollow : MonoBehaviour
    {
        private Transform target;
        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;

        //private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;

        private float zLoc;
        //private float defaultZlocation = 8;

        public float[] CameraZoomPoints;
        public float[] CameraZoomValues;

        private float currentTargetXLoc;

        private int counter;
        private int highestValueFound;

        public int deadZoneWidth = 5;
        
        // Use this for initialization
        private void Start()
        {

            target = GameObject.Find("Player").transform;

            m_LastTargetPosition = target.position;
            zLoc = -CameraZoomValues[0];
            transform.parent = null;
        }


        // Update is called once per frame
        private void Update()
        {

            //find out if camera needs to zoom in or out

            currentTargetXLoc = target.position.x;
            counter = 0;

            foreach (float i in CameraZoomPoints)                              

            {
                if (currentTargetXLoc > i)
                    highestValueFound = counter;
                else
                    break;
                counter++;

            }

            zLoc = - CameraZoomValues[highestValueFound];

            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - m_LastTargetPosition).x;

            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
            }

            Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward* zLoc;
			Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping*(Time.timeScale/2));

            //transform.position = newPos;

            float currentCameraXLoc = transform.position.x;

            float cameraMin = currentTargetXLoc - deadZoneWidth / 2;
            float cameraMax = currentTargetXLoc + deadZoneWidth / 2;

            if (currentCameraXLoc > cameraMax)
            {
                transform.position = new Vector3(cameraMax, newPos.y, newPos.z);
            }
            else if (currentCameraXLoc < cameraMin)
            {
                transform.position = new Vector3(cameraMin, newPos.y, newPos.z);
            }            
             
            m_LastTargetPosition = target.position;

        }
    }
}
