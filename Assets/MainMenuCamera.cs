﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class MainMenuCamera : MonoBehaviour {

    private GameObject MainCamera;

    // Use this for initialization
    void Start () {

        #if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT
        MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        MainCamera.GetComponent<DepthOfField>().enabled = true;
        #endif

    }
	
}
