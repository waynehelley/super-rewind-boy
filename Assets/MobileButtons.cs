﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class MobileButtons : MonoBehaviour {

    #if !UNITY_ANDROID && !UNITY_IPHONE && !UNITY_BLACKBERRY && !UNITY_WINRT

#else

    private GameObject player;

    //this is messy, how the ability button uses the character controller directly

    private LunaUserControl lunaUserControl;
    private LunaCharacterController lunaCharacterControl;

    private TimeControl timeControl;

    void Start()
    {


        player = GameObject.Find("Player"); //Player's Character
        lunaUserControl = player.GetComponent<LunaUserControl>();
        lunaCharacterControl = player.GetComponent<LunaCharacterController>();
        timeControl = FindObjectOfType<TimeControl>();

    }

    public void LeftButtonDown () {
        lunaUserControl.leftbuttonpress();	
	}

    public void RightButtonDown() {
        lunaUserControl.rightbuttonpress();
	}

    public void DirectionButtonUp()
    {
        lunaUserControl.buttonrelease();
    }

    public void TimeButtonDown()
    {
        timeControl.timebuttonpress();
    }

    public void TimeButtonUp()
    {
        timeControl.timebuttonrelease();
    }

    public void JumpButtonDown()
    {
        lunaUserControl.jumpButtonDown();
    }

    public void JumpButtonUp()
    {
        lunaUserControl.jumpButtonUp();
    }

    public void AttackButtonDown()
    {
        lunaCharacterControl.Ability(true);
    }

    public void AttackButtonUp()
    {
        lunaCharacterControl.Ability(false);
    }

#endif

}


