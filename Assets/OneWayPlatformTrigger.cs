﻿using UnityEngine;
using System.Collections;

public class OneWayPlatformTrigger : MonoBehaviour {

    public BoxCollider2D platform1;
    public BoxCollider2D platform2;    
    public BoxCollider2D playerParentingOnTriggerCollider;
    public bool oneWay = false;
    public bool playerParentingEnabled = true;

    public string collider; //eg. "Player", "Crate"
    
    
    // Update is called once per frame
    void Update()
    {
        platform1.enabled = !oneWay;        
        platform2.enabled = !oneWay;
        if (playerParentingOnTriggerCollider != null && playerParentingEnabled)
        {
            playerParentingOnTriggerCollider.enabled = !oneWay;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == collider)
            oneWay = true;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == collider)
            oneWay = true;
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == collider)
            oneWay = false;
    }

}
