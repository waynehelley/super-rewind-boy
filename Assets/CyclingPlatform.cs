﻿using UnityEngine;
using System.Collections;
using Chronos;

public class CyclingPlatform : MonoBehaviour
{

    private GameObject player;

    public int Marker = 0;
    private int WayPointLength;
    public int speed = 4;
    public Transform[] Waypoints;
    //public GameObject Trigger;
    public bool TimeControlled = true;
    public Color FlashColour;
    private Color OrigionalColour;    
    private SpriteRenderer BlurBorder;
    private Color BlurBorderColour;
    public bool Reversed = false;


    void Start()
    {


        player = GameObject.Find("Player"); //Player's Character
        WayPointLength = Waypoints.Length;
        GetComponent<Timeline>().enabled = TimeControlled;
        //Trigger.GetComponent<Timeline>().enabled = TimeControlled;


    }

    // Update is called once per frame
    void Update()
    {

        transform.position = Vector3.MoveTowards(transform.position, Waypoints[Marker].transform.position, speed * Time.deltaTime);

        if (!Reversed)
        {
            if (transform.position == Waypoints[Marker].transform.position)
            {
                if (Marker < WayPointLength)
                {
                    Marker++;
                }
            }
            if (Marker == Waypoints.Length)
            {
                Marker = 0;
            }
        }
        else
        {
            if (transform.position == Waypoints[Marker].transform.position)
            {
                if (Marker >= 0)
                {
                    Marker--;
                }
                
            }
            if (Marker == -1)
            {
                Marker = Waypoints.Length -1;
            }
        }

    }
    
}
