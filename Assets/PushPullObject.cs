﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushPullObject : MonoBehaviour {

    private bool isBeingPushed;
    private Rigidbody2D rb;

    private float playerXpos;
    private float playerYpos;

    private float pushVelocity = 2f;

    public Vector3 startLocalScale;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();

        //grab the local scale - to be used by PlayerParenting to reset scale
        startLocalScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {

        if (isBeingPushed)
        {

            if (playerYpos -1f < transform.position.y)
            { 
                if (playerXpos < transform.position.x)
                {
                    rb.velocity = new Vector2(pushVelocity, 0);
                }
                else
                {
                    rb.velocity = new Vector2(-pushVelocity, 0);
                }

            }
        }

	}

    //If character collides with the platform, make it its child.
    void OnTriggerEnter2D(Collider2D other)
    {
        //Here we must detect which direction the Player has momentum. Only MakeChild if the Player is travelling downwards!

        if (other.gameObject.tag == "Player") //and player is at side of crate
        {
            isBeingPushed = true;
            playerXpos = other.transform.position.x;
            playerYpos = other.transform.position.y;
        }
        
    }

    void OnTriggerExit2D(Collider2D other)
    {
        //Here we must detect which direction the Player has momentum. Only MakeChild if the Player is travelling downwards!

        if (other.gameObject.tag == "Player") //and player is at side of crate
        {
            isBeingPushed = false;
            rb.velocity = new Vector2(0, 0);
        }

    }

}
