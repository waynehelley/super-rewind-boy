﻿using UnityEngine;
using System.Collections;
using Chronos;

public class TimeImmune : MonoBehaviour
{

        
    public bool TimeControlled = true;
    public GameObject TimeImmuneGlow;
    
    void Start()
    {
        
        GetComponent<Timeline>().enabled = TimeControlled;
        
        TimeImmuneGlow.SetActive(!TimeControlled);

    }

   
}
