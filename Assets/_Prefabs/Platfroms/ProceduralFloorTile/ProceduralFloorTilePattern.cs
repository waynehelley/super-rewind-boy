﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ProceduralFloorTilePattern: MonoBehaviour
{

    public GameObject FloorTileMesh;
    public Material Grass;    

    public List<Material> DirtMaterials;

    private float remainingWidth;
    private float rowsCreated;

    /// Add a context menu named "Do Something" in the inspector
    /// of the attached script.
    [ContextMenu("Regenerate Floor Tile Pattern")]
       

    void RegenerateFloorTilePattern()
    {

        float width = transform.localScale.x;
        float height = transform.localScale.y;

        int safetyCount = 0;

        while (transform.childCount > 0 & safetyCount < 200)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);

            safetyCount++;
        }

        //grass layer
        GameObject go = Instantiate(FloorTileMesh, transform.position, Quaternion.identity);        
        go.transform.localScale = new Vector3(width, 0.5f, 1);
        go.transform.localPosition += new Vector3(0, height/2 - 0.25f, 0);
        go.transform.parent = transform;

        rowsCreated = 1;

        System.Random random = new System.Random();

        while (rowsCreated < height*2)                        
                
        {

            remainingWidth = width;                  

            while (remainingWidth > 0)
            {
                //dirt
                GameObject go1 = Instantiate(FloorTileMesh, transform.position, Quaternion.identity);
                               

                float dirt1length = random.Next(1, 5);
                
                dirt1length = Mathf.Round(dirt1length);

                if (remainingWidth < dirt1length)
                {
                    dirt1length = remainingWidth;
                }

                go1.transform.localScale = new Vector3(dirt1length, 0.5f, 1);
                go1.transform.localPosition += new Vector3(-width / 2 + dirt1length / 2 + (width-remainingWidth), height / 2 - 0.25f - rowsCreated/2, 0);
                go1.transform.parent = transform;

                int r = random.Next(DirtMaterials.Count);
                go1.transform.GetComponent<Renderer>().sharedMaterial = DirtMaterials[r];

                remainingWidth = remainingWidth - dirt1length;
            }

            rowsCreated++;

        }  

    }
}






