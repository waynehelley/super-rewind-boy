﻿using UnityEngine;
using System.Collections;

public class SwitchScript : MonoBehaviour {

    Animator anim;
    public bool sticks=true;
    public bool down = false;
    private bool playerEntered = false;
    private bool obsticleEntered = false;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();


	}
	
	// Update is called once per frame
	void Update () {
	
	}


    void OnTriggerStay2D(Collider2D other)
    {
        anim.SetBool("goDown", true);
        if (down == false)
        {
            AudioSource audio = GetComponent<AudioSource>();
            audio.Play();
        }
        down = true;

        if (other.tag == "Player")
        { playerEntered = true; }
        else
        { obsticleEntered = true; }

    }

    void OnTriggerExit2D(Collider2D other)
    {

        if (other.tag == "Player")
        { playerEntered = false; }
        else
        { obsticleEntered = false; }

        if (!sticks)
        {
            anim.SetBool("goDown", false);
            if (down == true & playerEntered == false && obsticleEntered == false)
            {
                AudioSource audio = GetComponent<AudioSource>();
                audio.Play();
            }
            down = false;
        }
        
    }

}
