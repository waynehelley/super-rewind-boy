﻿using UnityEngine;
using System.Collections;

public class CubeController : MonoBehaviour {


    public BoxCollider2D boxCol;
    private Animator m_Anim;
    private GameObject WallModel;

    // Use this for initialization
    void Start () {

        boxCol = GetComponent<BoxCollider2D>();
        m_Anim = GetComponentInChildren<Animator>();
        WallModel = this.gameObject.transform.GetChild(0).gameObject;

    }
	
	// Update is called once per frame
	void Update () {

		if(GetComponent<EnemyDestructibleHealth>() != null)
		{
			if(GetComponent<EnemyDestructibleHealth>().health <= 0)
			{
                //deactivate collider
                boxCol.enabled = false;

                //start animation
                m_Anim.SetBool("Collapsed",true);

                //Turn off child object which contains render
                StartCoroutine(WallDestroy());
            }

		}

	}

    IEnumerator WallDestroy()

    {
        
        yield return new WaitForSeconds(1f);
        WallModel.SetActive(false);

    }
}
