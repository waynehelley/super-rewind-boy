﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class ConstantMotion : MonoBehaviour {

    public float speed = 1;
    public Clock clock;

    void Start()
    { 
    clock = Timekeeper.instance.Clock("Enemies");
        }

    // Update is called once per frame
    void Update()
    {
        if (clock.localTimeScale > 0) //only move platfrom when time is moving forwards
        {
            transform.position = new Vector3(transform.position.x + Time.deltaTime * speed, transform.position.y, transform.position.z);
        }
       
    }
}
