﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProceduralClouds : MonoBehaviour {

    public List<GameObject> CloudPrefabs;
    public int numberOfClouds = 25;

    private int leftOfClouds = -1000;
    private int rightOfClouds = 1000;

    private int frontOfClouds = 800;
    private int rearOfClouds = 1300;

    private float cloudHeight = 425;

    // Use this for initialization
    void Start () {

        System.Random random = new System.Random();
        
        for (int i = 0; i <= numberOfClouds; i++)
        {
            int randomXLocation = random.Next(leftOfClouds, rightOfClouds);
            int randomZLocation = random.Next(frontOfClouds, rearOfClouds);

            int randomCloud = random.Next(CloudPrefabs.Count);
            
            Vector3 cloudPostion = new Vector3(randomXLocation, cloudHeight, randomZLocation);

            Quaternion cloudRotation = new Quaternion(0, 0, 0, 0);


            GameObject go1 = Instantiate(CloudPrefabs[randomCloud], cloudPostion, cloudRotation);
            
        }


    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
