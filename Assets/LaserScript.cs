﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Chronos;

public class LaserScript : MonoBehaviour {

    LineRenderer line;
    Light myLight;
    GameObject hitPointObject;
    public bool fireLaser;
    Death deathScript;

    public bool intermitent;
    public float timeOn =2f;
    public float timeOff =2f;
    public float cycleDelay = 5f;
    private float cycleTime;
    private float timeOffRatio;       

    private Clock clock;    

    // Use this for initialization
    void Start () {

        line = GetComponent<LineRenderer>();
        line.enabled = false;

        myLight = GetComponent<Light>();
        myLight.enabled = false;

        hitPointObject = transform.GetChild(0).gameObject;
        hitPointObject.SetActive(false);

        //Is is essential for the line to use worldspace for the FireLaser co-ordinate systems to be correct.
        //Leave turned off in editor to give preview of line direction.
        line.useWorldSpace = true;
        
        if(intermitent)
        {
            clock = Timekeeper.instance.Clock("Enemies");
            cycleTime = timeOff + timeOn;
            timeOffRatio = timeOff / cycleTime;            
        }
               

    }
	
	// Update is called once per frame
	void Update () {

        if (intermitent)
        {
            float clockTime = clock.time;

            float cycles = (clockTime -cycleDelay)/ cycleTime;
            float cyclePosition = cycles - Mathf.Floor(cycles);

            fireLaser = cyclePosition > timeOffRatio;
        }
        
        if (fireLaser)
        {
                       

            StopCoroutine("FireLaser");
            StartCoroutine("FireLaser");                      

        }

	}


    IEnumerator FireLaser()
    {
        line.enabled = true;
        myLight.enabled = true;
        hitPointObject.SetActive(true);

        while (fireLaser)
        {            

            //line.renderer.material.mainTextureOffset = new Vector2(0, Time.time);            
            line.GetComponent<Renderer>().sharedMaterial.mainTextureOffset = new Vector2(0, Time.time);

            Ray2D ray = new Ray2D(transform.position, transform.right);
           
            RaycastHit2D hit = Physics2D.Raycast(transform.position,transform.right);
            
            line.SetPosition(0, ray.origin);

            Vector2 hitPos = (hit.point);
            hitPointObject.transform.position = hitPos;

            if (hit.collider != null)
            {               
                
                line.SetPosition(1, hitPos);

                if(hit.collider.tag == "Player")
                {
                    deathScript = hit.collider.GetComponent<Death>();

                    deathScript.PlayerDeath();
                }

            }
            else
            { line.SetPosition(1, ray.GetPoint(50)); }

            yield return null;
            
        }

        line.enabled = false;
        myLight.enabled = false;
        hitPointObject.SetActive(false);

    }         

}
